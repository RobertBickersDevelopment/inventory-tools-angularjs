﻿using Garage.Assistant.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace Garage.Assistant.Web.Controllers
{
    public class InventoryController : BaseController
    {


        private static List<Product> _products = new List<Product>()
        {
              new Product(1, "Spanner", "Size 4 Spanner", 4, true, 55m),
                new Product(2, "Wrench", "Adjustable wrench", 2, true, 65.55m),
                new Product(3, "Hammer", "Good for whacking things", 0, false, 77.25m),
                new Product(4, "Jack Lift", "Great for lifting things", 1, true, 44.25m),
                new Product(5, "Spare Tyre", "Goes round", 4, false, 77m)
        };

        // GET: Inventory
        public ActionResult Index()
        {

            return View();
        }


        public ActionResult IndexVM()
        {

            InventoryViewModel model = new InventoryViewModel();
            model.Products = _products;

            return Json(model, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult Edit(Product model)
        {

            Thread.Sleep(3000);

            if (ModelState.IsValid)
            {
                if (model.Id == 0 || model.Id == null)
                {
                    _products.Add(model);
                }
                else
                {
                    int index = _products.IndexOf(_products.Single(x => x.Id == model.Id));

                    if (index >= 0)
                    {
                        _products[index] = model;
                    }
                }
            }
            return Json(model, JsonRequestBehavior.AllowGet);
        }


    }
}