﻿using Garage.Assistant.Web.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Garage.Assistant.Web.Controllers
{
    public class CustomerController : BaseController
    {

        private static List<Customer> _customers = new List<Customer>()
        {
            new Customer(1, "Robert Bickers", DateTime.Now.AddDays(-1000), new List<Vehicle>(), true, "01234566775"),
            new Customer(2, "James Dibble", DateTime.Now.AddDays(-10450), new List<Vehicle>(), true, "01234566775"),
            new Customer(3, "Robin Hood", DateTime.Now.AddDays(-1200), new List<Vehicle>(), true, "01234566775"),
            new Customer(4, "Jack Ripper", DateTime.Now.AddDays(-1045), new List<Vehicle>(), true, "01234566775"),
            new Customer(5, "Rhett Mcgloughlin", DateTime.Now.AddDays(-10), new List<Vehicle>(), true, "01234566775"),
            new Customer(6, "Link Neil", DateTime.Now.AddDays(-34), new List<Vehicle>(), true, "01234566775"),
            new Customer(7, "Timothy James", DateTime.Now.AddDays(-34), new List<Vehicle>(), true, "01234566775")
        };


        // GET: Customer
        public ActionResult Index()
        {

            return View();
        }


        public ActionResult IndexVM()
        {
            CustomerViewModel model = new CustomerViewModel();
            model.Customers = _customers;

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        // GET: Customer/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Customer/Edit/5
        [HttpPost]
        public ActionResult Edit(Customer model)
        {
            try
            {
                Debug.WriteLine("Customer is being added");

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Customer/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Customer/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
