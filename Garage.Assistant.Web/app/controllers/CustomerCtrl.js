﻿
angular.module('AngularDemo.CustomerController', [])
    .controller('CustomerCtrl', ['$scope', '$http', function ($scope, $http) {

        $scope.model = {};

        $scope.Title = "Customer";



        $scope.states = {
            detailView: false,
            selected: {}
        }


        $http.get('/Customer/IndexVM').success(function (data) {
            $scope.model = data;
        });

        $scope.showDetailsPage = function (customerId) {
            if (customerId === null || customerId === undefined) {
                $scope.states.selected = {}
                $scope.states.detailView = false;
            }
            else {
                $scope.states.selected = GetByCustomerId(customerId);
                $scope.states.detailView = true;
            }
            SetTitle();
        };


        function SetTitle() {
            if ($scope.states.selected !== {} && $scope.states.detailView) {
                $scope.Title = "Customer - " + $scope.states.selected.Name;
            }
            else {
                $scope.Title = "Customer";
            }
        }

        function GetByCustomerId(customerId) {
            var result = $.grep($scope.model.Customers, function (e) { return e.Id == customerId; });

            if (result.length == 0) {
                console.log("zero customers found with Id: " + customerId);
            } else if (result.length == 1) {
                return result[0];
            } else {
                console.log("multiple results for Id: " + customerId);
            }
        };

    }]);
