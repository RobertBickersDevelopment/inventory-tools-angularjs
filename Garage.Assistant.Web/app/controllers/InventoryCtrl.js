﻿
angular.module('AngularDemo.InventoryController', [])
    .controller("InventoryCtrl", ['$scope', '$http', function ($scope, $http) {
        $scope.model = {};
        $scope.states = {
            showAddForm: false,
            isAdding: false
        };

        $http.get('/Inventory/IndexVM').success(function (data) {
            $scope.model = data;
        });

        $scope.new = {
            Product: {}
        };

        $scope.showAddForm = function (show) {
            $scope.states.showAddForm = show;
        };


        $scope.addProduct = function () {

            $scope.states.isAdding = true;

            $http.post("/Inventory/Edit", $scope.new.Product).success(function (data) {
                $scope.states.isAdding = false;
                $scope.model.Products.push(data);
                $scope.showAddForm(false);
                $scope.new.Product = {};
            });
        };

    }]);
