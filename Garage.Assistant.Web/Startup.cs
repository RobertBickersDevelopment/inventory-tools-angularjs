﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Garage.Assistant.Web.Startup))]
namespace Garage.Assistant.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
