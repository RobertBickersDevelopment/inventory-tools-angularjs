﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Garage.Assistant.Web.Models
{
    public class Customer
    {
        public Customer()
        {

        }

        public Customer(int id, string name, DateTime creationDate, List<Vehicle> vehicles, bool isPriority, string phoneNumber)
        {
            Id = id;
            Name = name;
            CreationDate = creationDate;
            Vehicles = vehicles;
            IsPriority = isPriority;
            PhoneNumber = phoneNumber;
        }

        public int? Id { get; set; }
        public string Name { get; set; }
        public DateTime CreationDate { get; set; }
        public IEnumerable<Vehicle> Vehicles { get; set; }
        public bool IsPriority { get; set; }
        public string PhoneNumber { get; set; }

    }
}