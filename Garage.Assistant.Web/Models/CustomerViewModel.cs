﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garage.Assistant.Web.Models
{
    public class CustomerViewModel
    {
        public CustomerViewModel()
        {

        }
        public CustomerViewModel(IEnumerable<Customer> customers)
        {
            Customers = customers;
        }


        public IEnumerable<Customer> Customers { get; set; }

    }
}
