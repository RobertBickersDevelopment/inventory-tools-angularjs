﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garage.Assistant.Web.Models
{
    public class Product
    {
        public Product()
        {
        }

        public Product(int id, string name, string description, int quantity, bool inStock, decimal price)
        {
            Id = id;
            Name = name;
            Description = description;
            InStock = inStock;
            Quantity = quantity;
            Price = price;
        }

        public int? Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool InStock { get; set; }
        public int Quantity { get; private set; }
        public decimal Price { get; set; }
    }
}
