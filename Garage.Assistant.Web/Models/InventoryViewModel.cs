﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garage.Assistant.Web.Models
{
    public class InventoryViewModel
    {
        public InventoryViewModel()
        {

        }

        public InventoryViewModel(IEnumerable<Product> products)
        {
            Products = products;
        }

        public IEnumerable<Product> Products { get; set; }

    }
}
